package utils;


public class RentalSerialNumber {
    private static int id;
    private static RentalSerialNumber rentalSerialNumber = null;
    private RentalSerialNumber(){}
    public static RentalSerialNumber getRentalSerialNumber(){
        if(rentalSerialNumber == null){
            rentalSerialNumber = new RentalSerialNumber();
        }
        return rentalSerialNumber;
    }
    public int getId(){
        return ++id;
    }
}