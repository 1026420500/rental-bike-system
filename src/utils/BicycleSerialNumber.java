package utils;

import bean.BikeType;



public class BicycleSerialNumber {
    private static BicycleSerialNumber bicycleSerialNumber = null;
    private String letters;
    private static int digit = 300;
    private  BicycleSerialNumber(){
    }
    private BicycleSerialNumber(String letters){
        if(BikeType.ELECTRIC.equals(letters)){
            this.letters = "ae";
        }else {
            this.letters = "ab";
        }

    }
    public String getDigit() {
        return String.valueOf(digit);
    }

    public String getLetters() {
        return letters;
    }
    public String getBicycleCode(){
        return letters + ++digit;
    }
    public static BicycleSerialNumber getBicycleSerialNumber(String letters){
        if(bicycleSerialNumber == null || !bicycleSerialNumber.getLetters().equals(letters)){
            bicycleSerialNumber = new BicycleSerialNumber(letters);
        }
        return bicycleSerialNumber;
    }
}