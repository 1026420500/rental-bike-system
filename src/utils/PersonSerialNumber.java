package utils;


public class PersonSerialNumber {
    private static int personNumber = 0;
    private static PersonSerialNumber personSerialNumber = null;
    private PersonSerialNumber(){

    }
    public static PersonSerialNumber getPersonSerialNumber(){
        if(personSerialNumber == null){
            personSerialNumber = new PersonSerialNumber();
        }
        return personSerialNumber;
    }
    public String getPersonNumber(){
        String pre = "0";
        ++personNumber;
        String number = String.valueOf(personNumber);
        if(personNumber <= 9){
            number = pre + number;
        }
        return number;
    }
}