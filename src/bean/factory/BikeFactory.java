package bean.factory;

import bean.Bike;
import bean.BikeType;
import bean.impl.ElectricBike;
import bean.impl.RoadBike;



public class BikeFactory {
    public static Bike getInstance(String bikeType, String serialNumber){
        if(BikeType.MANUAL.equals( bikeType)){
            //Road Bicycle Type and Road Bicycle Serial Number ，return the newly created Road Bicycle object
            return (Bike)new RoadBike(bikeType,serialNumber);
        }else {
            //The e-bike type and e-bike serial number， returns the newly created e-bike object
            return (Bike)new ElectricBike(bikeType,serialNumber);
        }
    }
}