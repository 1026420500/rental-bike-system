package bean.relationentity;

import bean.Bike;
import bean.impl.CustomerRecord;


public class RentBikeInfo {
    private int id;
    private CustomerRecord customerRecord;
    private Bike bike;
    private int rentalDays;
    private double amount;

    public int getId() {
        return id;
    }

    public CustomerRecord getCustomerRecord() {
        return customerRecord;
    }

    public Bike getBike() {
        return bike;
    }

    public int getRentalDays() {
        return rentalDays;
    }

    public double getAmount() {
        return amount;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setCustomerRecord(CustomerRecord customerRecord) {
        this.customerRecord = customerRecord;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public void setRentalDays(int rentalDays) {
        this.rentalDays = rentalDays;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "RentBikeInfo{" +
                "id=" + id +
                ", customerRecord=" + customerRecord +
                ", bike=" + bike +
                ", rentalDays=" + rentalDays +
                ", amount=" + amount +
                '}';
    }
}