package bean.impl;

import bean.CustomerType;
import bean.Person;

import java.text.SimpleDateFormat;
import java.util.Date;


public class CustomerRecord extends Person implements CustomerType {
    private String customerNumber;
    private Date recordIssuedYear;
    private String customerType = NORMAL;
    public CustomerRecord(){
        super();
    }
    public CustomerRecord(String customerName, Date customerBirthday, String customerNumber, Date recordIssuedYear) {
        super(customerName,customerBirthday);
        this.customerNumber = customerNumber;
        this.recordIssuedYear = recordIssuedYear;
    }
    public String getAge(){
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
        int issuedYear = Integer.valueOf(sdf1.format(recordIssuedYear));
        int birthYear = Integer.valueOf(sdf2.format(super.getBirthday()).split("/")[0]);
        return String.valueOf(issuedYear-birthYear);
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public Date getRecordIssuedYear() {
        return recordIssuedYear;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setRecordIssuedYear(Date recordIssuedYear) {
        this.recordIssuedYear = recordIssuedYear;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return "CustomerRecord{" +
                "customerName='" + super.getName()+ '\'' +
                ", customerBirthday=" + sdf.format(super.getBirthday())+
                ", customerNumber='" + customerNumber + '\'' +
                ", recordIssuedYear=" + sdf.format(recordIssuedYear) +
                ",customerType=" + customerType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CustomerRecord that = (CustomerRecord) o;

        if (super.getName() != null ? !super.getName().equals(that.getName()) : that.getName() != null) return false;
        if (super.getBirthday() != null ? !super.getBirthday().equals(that.getBirthday()) : that.getBirthday() != null)
            return false;
        if (customerNumber != null ? !customerNumber.equals(that.customerNumber) : that.customerNumber != null)
            return false;
        if (recordIssuedYear != null ? !recordIssuedYear.equals(that.recordIssuedYear) : that.recordIssuedYear != null)
            return false;
        return customerType != null ? customerType.equals(that.customerType) : that.customerType == null;
    }

}