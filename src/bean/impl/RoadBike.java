package bean.impl;

import bean.Bike;



public class RoadBike extends Bike {
    private double price = 10.0;//The cost of renting for one day
    public RoadBike(String bikeType,String serialNumber){
        super(bikeType,serialNumber);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}