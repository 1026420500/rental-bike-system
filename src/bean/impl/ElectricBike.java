package bean.impl;

import bean.Bike;



public class ElectricBike extends Bike {
    private String electricity = "fullCharged";//The initialization defaults to full power
    private Double price = 40.0;//The cost of renting for one day
    public ElectricBike(){
        super();
    }
    public ElectricBike(String bikeType, String serialNumber){
        super(bikeType,serialNumber);
    }
    public void setElectricityInUse(){
        this.electricity = "notFullCharged";
    }
    public void setElectricityFull(){
        this.electricity = "fullCharged";
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}