package bean;



public abstract class Bike {
    private String bikeType;//Type of bike (electric or road)
    private String bikeSerialNumber;//Bicycle serial number
    private int rentStatus;//Default is 0, that is, not borrowed;A value of 1 indicates borrowed
    public Bike(){
    }
    protected Bike(String bikeType,String bikeSerialNumber) {
        this.bikeType = bikeType;
        this.bikeSerialNumber = bikeSerialNumber;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }

    public void setBikeSerialNumber(String serialNumber){
        this.bikeSerialNumber = serialNumber;
    }

    public String getBikeSerialNumber() {
        return bikeSerialNumber;
    }

    public int getRentStatus() {
        return rentStatus;
    }

    public void setRentStatus(int rentStatus) {
        this.rentStatus = rentStatus;
    }

    @Override
    public String toString() {
        String status = null;
        status = rentStatus == 0? "can rent" :"Has been rented";
        return "Bike{" +
                "bikeType='" + bikeType + '\'' +
                ", bikeSerialNumber='" + bikeSerialNumber + '\'' +
                ", rentStatus=" + status +
                '}';
    }
}