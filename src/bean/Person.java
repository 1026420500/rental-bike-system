package bean;

import java.util.Date;


public abstract class Person {
    private String name;//name
    private String age;//age
    private Date birthday;//birthday
    public Person() {

    }
    protected Person(String name){
        this.name = name;
    }
    protected Person(String name, Date birthday){
        this.name = name;
        this.birthday = birthday;
    }
    protected Person(String name, String age){
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (age != null ? !age.equals(person.age) : person.age != null) return false;
        return birthday != null ? birthday.equals(person.birthday) : person.birthday == null;
    }
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}