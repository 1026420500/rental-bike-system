import bean.*;
import bean.factory.BikeFactory;
import bean.relationentity.RentBikeInfo;
import bean.impl.CustomerRecord;
import bean.impl.ElectricBike;
import bean.impl.RoadBike;
import datalist.BikeList;
import datalist.CustomerList;
import datalist.RentalBikeInfoList;
import utils.BicycleSerialNumber;
import utils.PersonSerialNumber;
import utils.RentalSerialNumber;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;



public class Test {

    public static int availableBikes(String typeOfBike){
        int count = 0;
        for (int i = 0; i < BikeList.getBikeArrayList().size(); i ++){
            if(BikeList.getBikeArrayList().get(i).getBikeType().equals(typeOfBike)){
                if(BikeList.getBikeArrayList().get(i).getRentStatus() == 0){
                    count++;
                }
            }
        }
        return count;
   }

    /**
     * @return A collection of all borrowed bicycles
     */
    public static ArrayList<Bike> getRentedBikes(){
        ArrayList<Bike> bikeArrayList = new ArrayList<>();
        for (int i = 0; i < BikeList.getBikeArrayList().size(); i ++){
            if(BikeList.getBikeArrayList().get(i).getRentStatus() == 1){
                bikeArrayList.add(BikeList.getBikeArrayList().get(i));
            }
        }
        return bikeArrayList;
    }

    /**
     * @return A collection of all bicycles available for loan
     */
    public static ArrayList<Bike> getNoRentedBikes(){
        ArrayList<Bike> bikeArrayList = new ArrayList<>();
        for (int i = 0; i < BikeList.getBikeArrayList().size(); i ++){
            if(BikeList.getBikeArrayList().get(i).getRentStatus() == 0){
                bikeArrayList.add(BikeList.getBikeArrayList().get(i));
            }
        }
        return bikeArrayList;
    }

    /**
     * @param customerRecord
     * @return A bicycle that a customer is borrowing
     */
    public Bike getBike(CustomerRecord customerRecord){
        Bike bike = null;
        for (int i = 0; i < RentalBikeInfoList.getRentBikeInfoArrayList().size(); i ++){
            if(customerRecord != null && customerRecord.getCustomerNumber().equals(RentalBikeInfoList.getRentBikeInfoArrayList().get(i).getCustomerRecord().getCustomerNumber())){

                bike = RentalBikeInfoList.getRentBikeInfoArrayList().get(i).getBike();
            }
        }
        return bike;
    }


    public static String issueBike(CustomerRecord customerRecord, String typeOfBike) {
        ArrayList<Bike> electricBike = new ArrayList<>();
        ArrayList<Bike> manualBike = new ArrayList<>();
        for (int i = 0; i < BikeList.getBikeArrayList().size(); i++) {
            if (BikeType.ELECTRIC.equals(BikeList.getBikeArrayList().get(i).getBikeType())) {
                electricBike.add(BikeList.getBikeArrayList().get(i));
            } else {
                manualBike.add(BikeList.getBikeArrayList().get(i));
            }
        }
        try {
            for (int i = 0; i < CustomerList.getCustomerRecordArrayList().size(); i++) {
                if (CustomerList.getCustomerRecordArrayList().get(i).getCustomerNumber().equals(customerRecord.getCustomerNumber())) {
                    //You can continue if the customer number given an input exists in the customer set
                    //1.2 If it is valid, check whether the number has been borrowed and rented. One person can only rent one bike
                    for (int k = 0; k < RentalBikeInfoList.getRentBikeInfoArrayList().size(); k ++){
                        if(customerRecord.equals(RentalBikeInfoList.getRentBikeInfoArrayList().get(k).getCustomerRecord())){
                            System.out.println("This number has been rented, please return it and rent it again");
                            return "a bike cannot be issued";
                        }
                    }
                    if (BikeType.ELECTRIC.equals(typeOfBike)) {//If you borrow a trolley, check if you are older than 21
                        if (Integer.valueOf(customerRecord.getAge()) >= 21 && customerRecord.getCustomerType().equals(CustomerType.GOLD)) {//如年龄满足条件且是黄金会员，则看是否存在没被借走的电车。
                            //Check the bike collection for trolley bikes that have not been checked out
                            for (int j = 0; j < electricBike.size(); j++) {
                                if (electricBike.get(j).getRentStatus() == 0) {
                                    //Modify the tram concession status and battery status
                                    electricBike.get(j).setRentStatus(1);
                                    ((ElectricBike)electricBike.get(j)).setElectricityInUse();
                                    //Creates a record in the rental information and saves it to the corresponding collection
                                    RentBikeInfo rentBikeInfo = new RentBikeInfo();
                                    rentBikeInfo.setId(RentalSerialNumber.getRentalSerialNumber().getId());
                                    rentBikeInfo.setCustomerRecord(customerRecord);
                                    rentBikeInfo.setBike(electricBike.get(j));
                                    //Assume the rental time is three days
                                    rentBikeInfo.setRentalDays(3);
                                    Double amount = ((ElectricBike)electricBike.get(j)).getPrice() * 3;//Total amount to be paid
                                    rentBikeInfo.setAmount(amount);
                                    RentalBikeInfoList.getRentBikeInfoArrayList().add(rentBikeInfo);//Add the rental record to the corresponding collection
                                    return  customerRecord.getName()+"The electric bike with the number"+ electricBike.get(j).getBikeSerialNumber()+"is successfully rented";
                                }
                            }
                        }
                    } else {//If it's a regular road bike
                        for (int j = 0; j < manualBike.size(); j++) {
                            if (manualBike.get(j).getRentStatus() == 0) {
                                //The user number has been checked before, and the user at this time is a valid user and has no rental record;As long as you traverse to a car that has not been rented, you can start renting.
                                manualBike.get(j).setRentStatus(1);//Change Concession status to 1 to indicate Borrowed
                                RentBikeInfo rentBikeInfo = new RentBikeInfo();
                                rentBikeInfo.setId(RentalSerialNumber.getRentalSerialNumber().getId());
                                rentBikeInfo.setCustomerRecord(customerRecord);
                                rentBikeInfo.setBike(manualBike.get(j));
                                //Assume the length of the lease is four days
                                Double amount = ((RoadBike)manualBike.get(j)).getPrice() * 4;
                                rentBikeInfo.setRentalDays(4);
                                rentBikeInfo.setAmount(amount);
                                RentalBikeInfoList.getRentBikeInfoArrayList().add(rentBikeInfo);
                                return customerRecord.getName()+"The road bike with the number"+manualBike.get(j).getBikeSerialNumber()+"is successfully rented";
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "a bike cannot be issued";
    }

    /**
     * @param customerRecord
     * @description Return a car that a customer is borrowing
     */
    public static String terminateRental(CustomerRecord customerRecord){
        //Delete the lease record (that is, from the rental collection RentalBikeInfolist)
        for(int i = 0; i < RentalBikeInfoList.getRentBikeInfoArrayList().size(); i ++){
            if(customerRecord != null && customerRecord.getCustomerNumber().equals(RentalBikeInfoList.getRentBikeInfoArrayList().get(i).getCustomerRecord().getCustomerNumber())){
                //If the user number is the same in the concession record list, the user has a rental car.
                RentalBikeInfoList.getRentBikeInfoArrayList().get(i).getBike().setRentStatus(0);//Change the bike loan status to not checked out
                if(RentalBikeInfoList.getRentBikeInfoArrayList().get(i).getBike() instanceof ElectricBike){
                    //If the trolley is returned, it needs to be fully charged after it is returned
                    ((ElectricBike)RentalBikeInfoList.getRentBikeInfoArrayList().get(i).getBike()).setElectricityFull();
                }
                RentalBikeInfoList.getRentBikeInfoArrayList().remove(i);
                return "Returned successfully";
            }
        }
        return "Return failed";
    }

    /**
     *
     * @param args
     * @throws ParseException
     * @throws FileNotFoundException
     * @description Test methods to test the functionality of various interfaces, classes, and methods
     */
    public static void main(String[] args) throws ParseException, FileNotFoundException {
        insantiateBike();// //Instantiate 50 bikes in the Bikelist
        insantiateCustomerRecord();//Instantiate four valid users in the CustomerList
        boolean flag = true;
        Scanner scanner = new Scanner(System.in);
        while (flag){
            System.out.println("Please enter the operation instruction：1.Query valid user information；2，Inquire rental vehicle information；3，Inquire two car rental information；4，Rent a bike；5，Return the bike；6.Rent a bike to record information；7，Information on all the bikes；8，exit");
            switch (scanner.next()){
                case "1":
                    System.out.println(1);
                    for(int k = 0; k < CustomerList.getCustomerRecordArrayList().size(); k ++){
                        System.out.println(CustomerList.getCustomerRecordArrayList().get(k));
                    }
                    break;
                case "2":
                    System.out.println(2);
                    List waitRentList = getNoRentedBikes();
                    for (int j = 0; j < waitRentList.size(); j ++ ){
                        System.out.println(waitRentList.get(j));
                    }
                    break;
                case "3":
                    System.out.println(3);
                    List RentedList = getRentedBikes();
                    for (int l = 0; l < RentedList.size(); l ++ ){
                        System.out.println(RentedList.get(l));
                    }
                    break;
                case "4":
                    System.out.println(4);
                    System.out.println("Way of renting bicycles：1，random；2，Specify the bicycle serial number");
                    String rentalWay = scanner.next();
                    if(rentalWay.equals("1")){
                        System.out.println("Please enter the subscript of one of the four valid users，They correspond to 0,1,2,3");
                        int index = scanner.nextInt();
                        CustomerRecord customerRecord = CustomerList.getCustomerRecordArrayList().get(index);
                        String typeOfBike = BikeType.MANUAL;
                        System.out.println(issueBike(customerRecord, typeOfBike));
                    }else if(rentalWay.equals("2")){
                        System.out.println("Please enter the subscript of one of the four valid users，They correspond to 0,1,2,3");
                        int index = scanner.nextInt();
                        CustomerRecord record = CustomerList.getCustomerRecordArrayList().get(index);
                        System.out.println("Please enter the serial number");
                        String bikeSerialNumber = scanner.next();
                        System.out.println("Please enter rental days");
                        int day = scanner.nextInt();
                        System.out.println(rentBikeBySerialNumber(record,bikeSerialNumber,day));
                    }
                    break;
                case "5":
                    System.out.println(5);
                    CustomerRecord customer = CustomerList.getCustomerRecordArrayList().get(3);
                    System.out.println(terminateRental(customer));
                    break;

                case "6":
                    System.out.println(6);
                    for(int i = 0; i < RentalBikeInfoList.getRentBikeInfoArrayList().size(); i ++){
                        System.out.println(RentalBikeInfoList.getRentBikeInfoArrayList().get(i));
                    }
                    break;
                case "7":
                    System.out.println("All bike information");
                    System.out.println(7);
                    for(int m = 0; m < BikeList.getBikeArrayList().size(); m ++) {
                        System.out.println(BikeList.getBikeArrayList().get(m));
                    }
                    break;
                case "8":
                    flag = false;
                    System.out.println(8);
                    break;
                default:
                    System.out.println("Incorrect input, please reenter the digital instruction");
                    break;
            }
        }
    }

    /**
     *
     * @param record A valid user
     * @param serialNumber Bicycle serial number
     * @param day Number of days of lease
     * @return String prompt for rental result
     */
    private static String rentBikeBySerialNumber(CustomerRecord record, String serialNumber,int day) {
        for (int i = 0; i < BikeList.getBikeArrayList().size(); i ++){
            if(serialNumber != null && serialNumber.equals(BikeList.getBikeArrayList().get(i).getBikeSerialNumber())){
                if(BikeList.getBikeArrayList().get(i).getRentStatus() == 0){
                    BikeList.getBikeArrayList().get(i).setRentStatus(1);
                    RentBikeInfo rentBikeInfo = new RentBikeInfo();
                    rentBikeInfo.setId(RentalSerialNumber.getRentalSerialNumber().getId());
                    rentBikeInfo.setCustomerRecord(record);
                    rentBikeInfo.setBike(BikeList.getBikeArrayList().get(i));
                    //Determine whether the serial number of the bicycle corresponds to an electric bike or a bicycle.
                    if(BikeList.getBikeArrayList().get(i).getBikeSerialNumber().charAt(1) =='e'){
                     //Charge by electric bike
                       Double amount =  ((ElectricBike)BikeList.getBikeArrayList().get(i)).getPrice() * day;
                       rentBikeInfo.setRentalDays(day);
                       rentBikeInfo.setAmount(amount);
                    }else {
                        //Charge by bicycle
                        Double amount =  ((RoadBike)BikeList.getBikeArrayList().get(i)).getPrice() * day;
                        rentBikeInfo.setRentalDays(day);
                        rentBikeInfo.setAmount(amount);
                    }
                    RentalBikeInfoList.getRentBikeInfoArrayList().add(rentBikeInfo);
                    return "Bicycle rental successful";
                }
            }
        }
        return "Failure to rent a bike";
    }

    /**
     *
     * @throws FileNotFoundException
     * @throws ParseException
     * @description Instance the user records in the user.txt text into the program and change the identities of two of the users to Gold Membership
     */
    public static void insantiateCustomerRecord() throws FileNotFoundException, ParseException {
        //1. Name;2. Birthday;3. Record the year of generation;4 customer number (initials + year + sequence number);5. Rank (for Zoe Brown and John Smith, change to Gold status)
        Scanner inFile = new Scanner(new FileReader("D:\\workspace\\RentalBikeSystem\\users.txt"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");//The date format of the birthday
        Calendar c = Calendar.getInstance();//A calendar that records the year in which it was produced
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");//Generates the date format for the year
        while (inFile.hasNext()){
            String[] dataArray = inFile.nextLine().split("_");
            String personName = dataArray[0];
            String customerName = personName.split(",")[0] + " " + personName.split(",")[1];
            Date customerBirthday = sdf.parse(dataArray[1]);
            Date recordIssuedYear = sdf2.parse(String.valueOf(c.get(Calendar.YEAR)));
            String abbreviation = String.valueOf(personName.split(",")[0].charAt(0)) + personName.split(",")[1].charAt(0);
            String serialNumber = PersonSerialNumber.getPersonSerialNumber().getPersonNumber();
            String customerNumber = abbreviation + "-" + sdf2.format(recordIssuedYear) + "-" + serialNumber;
            CustomerRecord customerRecord1 = new CustomerRecord(customerName,customerBirthday,customerNumber,recordIssuedYear);
            CustomerList.getCustomerRecordArrayList().add(customerRecord1);
        }
        for(int i = 0; i < CustomerList.getCustomerRecordArrayList().size(); i ++){
            if(CustomerList.getCustomerRecordArrayList().get(i).getName().equals("John Smith")){
                CustomerList.getCustomerRecordArrayList().get(i).setCustomerType(CustomerType.GOLD);
            }
            if(CustomerList.getCustomerRecordArrayList().get(i).getName().equals("Zoe Brown")){
                CustomerList.getCustomerRecordArrayList().get(i).setCustomerType(CustomerType.GOLD);
            }
        }
    }

    /**
     * @description Instantiate 50 bikes
     */
    public static void insantiateBike(){
        for(int i = 0; i < 50; i ++){
            if(i < 10){
                //Electric bicycle AE301-AE310 is instantiated
                Bike bike = BikeFactory.getInstance(BikeType.ELECTRIC,BicycleSerialNumber.getBicycleSerialNumber(BikeType.ELECTRIC).getBicycleCode());
                BikeList.getBikeArrayList().add(bike);
            }else {
                //Instantiate the road bike AB311-AB350
                Bike bike = BikeFactory.getInstance(BikeType.MANUAL,BicycleSerialNumber.getBicycleSerialNumber(BikeType.MANUAL).getBicycleCode());
                BikeList.getBikeArrayList().add(bike);
            }
        }
    }

}
